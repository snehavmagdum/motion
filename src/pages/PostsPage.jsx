import Container from "../components/Layout/Container";
import SearchBar from "../components/Searchbar/Searchbar";
import PostContainer from "../components/Postpage/PostContainer";

const PostsPage = () => {
  return (
    <Container>
      <h1>App</h1>
      <SearchBar />
      <PostContainer />
    </Container>
  );
};

export default PostsPage;
