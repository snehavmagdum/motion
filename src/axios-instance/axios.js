import axios from "axios";

const baseUrl = "https://motion.propulsion-home.ch/backend/api";
const token = localStorage.getItem("accessToken");

export const axiosMotion = axios.create({
  baseURL: baseUrl,
  headers: {
    Authorization: `Bearer ${token}`,
    "Content-Type": "application/json",
  },
});
