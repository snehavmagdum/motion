import styled from "styled-components";
import { colors, shadows, textsSizes } from "../commonStyles/commonStyles";

export const DefaultPostColumn = styled.div`
  display: flex;
  align-content: center;
  flex-direction: column;
  width: 100%;
  padding: 1rem;
`;

export const DefaultWritePost = styled.div`
  display: flex;
  align-items: center;
  height: 5rem;
  width: 100%;
  background-color: ${colors.mainWhite};
  padding: 1rem;
  box-shadow: ${shadows.profileCard};
  border-radius: 4px;
`;

export const DefaultModalWritePost = styled.div`
  display: flex;
  align-items: center;
  height: 5rem;
  width: 500%;
  background-color: ${colors.mainWhite};
  padding: 1rem;
  box-shadow: ${shadows.profileCard};
  border-radius: 4px;
`;

export const DefaultPostMainContainer = styled.div`
  /* border: red dotted; */
  display: flex;
  justify-content: center;
  align-items: flex-start;
  flex-direction: row;
  margin-top: 2rem;
`;

export const DefaultUserPicturePost = styled.div`
  background-color: ${colors.mainWhite};
  display: flex;
  flex-direction: column;
  padding: 1rem;
  margin-top: 1rem;
  box-shadow: ${shadows.profileCard};
  border-radius: 4px;
`;

export const DefaultPicNameNow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  gap: 1rem;
  margin-bottom: 1.2rem;
`;

export const DefaultUserNameTiming = styled.div`
  font-family: "Roboto";
  font-style: normal;
  font-weight: 400;
`;

export const DefaultUserName = styled.p`
  font-size: ${textsSizes.paragraph};
  font-size: ${textsSizes.paragraph};
  display: flex;
  color: ${colors.mainBlack};
  cursor: pointer;
`;

export const DefaultPostTiming = styled.p`
  font-size: ${textsSizes.paragraph};
  color: ${colors.fadedBlack};
  display: flex;
`;

export const DefaultPostText = styled.p`
  color: ${colors.mainBlack};
  padding-bottom: 1rem;
`;

export const DefaultUserStatus = styled.div`
  display: flex;
  font-size: 1rem;
  line-height: 1.5rem;
  text-align: Left;
  font-family: "Roboto";
  font-style: normal;
  font-weight: 400;
  color: ${colors.mainBlack};
`;

export const DefaultPictureGrid = styled.div`
  display: grid;
  grid-template-rows: repeat(2, 1fr);
  grid-template-columns: repeat(2, 1fr);
  width: 100%;
  height: 100%;
  padding-top: 1rem;
  gap: 1rem;
`;

export const DefaultSinglePictureStyle = styled.img`
  width: 100%;
  cursor: pointer;
`;

export const DefaultPostFooter = styled.div`
  padding-top: 1rem;
  display: flex;
  flex-direction: row;
`;

export const DefaultSharingIcons = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 1.5rem;
  font-family: Roboto;
  font-size: 0.8rem;
`;

export const DefaultLikeDuo = styled.div`
  display: flex;
  flex-direction: row;
  gap: 1.75rem;
`;

export const DefaultShareDuo = styled.div`
  display: flex;
  flex-direction: row;
  margin-left: 1.5rem;
  gap: 1.75rem;
`;

export const DefaultLikeBlock = styled.div`
  display: flex;
  justify-content: end;
  margin-left: 12rem;
  color: #000000;
  opacity: 0.5;
`;

export const DefaultPostLikes = styled.span`
  /* border: red dotted; */
  display: flex;
  justify-content: flex-end;
`;

export const DefaultLikeButton = styled.span`
  cursor: pointer;
  background-color: transparent;
  border: none;
`;

export const DefaultDarkBackground = styled.div`
  background-color: rgba(0, 0, 0, 0.2);
  width: 100vw;
  height: 100vh;
  z-index: 0;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  position: absolute;
`;

export const DefaultCenteredModal = styled.div`
  position: fixed;
  background-color: white;
  height: 55%;
  padding: 2%;
  top: 50%;
  left: 50%;
  transform: translate(-70%, -70%);
  background-color: ${colors.mainWhite};
  box-shadow: ${shadows.profileCard};
  border-radius: 4px;
`;

export const DefaultModalThumbWhats = styled.div`
  display: flex;
  align-items: flex-start;
  flex-direction: row;
  gap: 1rem;
  width: 100%;
  height: 80%;
`;

export const DefaultModalFooter = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-top: 1px solid grey;
  margin-bottom: 5%;
  padding: 3%;
`;

export const DefaultbuttonModalArrowBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  border-top: 1px solid grey;
  border: 2px;
`;
