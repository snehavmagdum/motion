import axios from "axios";

const Base_url = "https://motion.propulsion-home.ch/backend/api";

export const AxiosMotion = axios.create({
  baseURL: Base_url,
  headers: {
    Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
    "Content-Type": "application/json",
  },
});

export const AxiosInstance = axios.create({
  baseURL: Base_url,
});

export function updateAxiosConfig(token) {
  AxiosMotion.defaults.headers.Authorization = `Bearer ${token}`;
  //   console.log("working", AxiosMotion.defaults.headers);
}
