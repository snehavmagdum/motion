import { createSlice } from "@reduxjs/toolkit";

const UserSlice = createSlice({
  name: "user",
  initialState: {
    accessToken: undefined,
    email: "",
  },

  reducers: {
    loginUser: (state, action) => {
      state.accessToken = action.payload;
    },
    logoutUser: (state) => {
      state.accessToken = null;
    },
    setemail: (state, action) => {
      state.email = action.payload;
    },
  },
});

export const { loginUser, logoutUser, setemail } = UserSlice.actions;

export default UserSlice.reducer;
