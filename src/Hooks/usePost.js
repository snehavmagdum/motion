import { useState, useEffect } from "react";
import { axiosMotion } from "../axios-instance/axios";

export const useFetch = endPoint => {
  const [date, setData] = useState();
  useEffect(() => {
    axiosMotion.post(endPoint).then(res => setData(res.data));
  }, []);
  return date;
};
