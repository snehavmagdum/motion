//### Import {useFetch} to your component
//Example const data = useFetch("/users/"), console.log(data) and start use the data :)

import { useState, useEffect } from "react";
import { axiosMotion } from "../axios-instance/axios";

export const useFetch = endPoint => {
  const [date, setData] = useState();
  useEffect(() => {
    axiosMotion.get(endPoint).then(res => setData(res.data));
  }, []);
  return date;
};
