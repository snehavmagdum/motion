export const colors = {
  mainBlack: "#000000",
  mainWhite: "#ffffff",
  mainPurple: "#a580ff",
  backgroundGray: "#F8F8F9",
  buttonGray: "#F2F2F2",
  fadedBlack: "rgba(0, 0, 0, 0.20)",
  black50: "rgba(0, 0, 0, 0.50)",
};

export const spacing = {
  profileInnerBoxesPaddings: "3.5rem",
  formRowSpacing: "2rem",
};

export const gradients = {
  mainButtonGradient: "linear-gradient(132.96deg, #C468FF 3.32%, #6E91F6 100%)",
};
export const textsSizes = {
  paragraph: "1rem",
  h3: { fontSize: "1.5rem", fontWeight: 400 },
};

export const shadows = {
  profileCard:
    "0px 0px 1px rgba(0, 0, 0, 0.2),0px 10px 20px rgba(0, 0, 0, 0.05)",
};
