import "../../index.css";

const ContainerWrapper = styled.div`
  margin-left: auto;
  margin-right: auto;
  padding: 0 1rem;
  width: 60;
  @media (min-width: 768px) {
    width: 750px;
  }
  @media (min-width: 992px) {
    width: 970px;
  }
  @media (min-width: 1200px) {
    width: 1170px;
  }
`;

const Container = (props) => {
  return <ContainerWrapper>{props.children}</ContainerWrapper>;
};

export default Container;
