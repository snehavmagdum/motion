import styled from "styled-components";
import { textsSizes } from "../../commonStyles/commonStyles";

const TextContent = styled.p`
  font-size: ${props => props.fontSize || textsSizes.paragraph};
  font-weight: ${props => props.fontWeight || null};
  text-align: ${props => props.textAlign || null};
  top: 50%;
  left: 50%;
`;

const Loading = () => {
  return <TextContent>Loading...</TextContent>;
};

export default Loading;
