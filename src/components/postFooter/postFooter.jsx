import { useState } from "react";
import "../Postpage/styles.css";
import heartLike from "../../motion-assets/svgs/heart.svg";
import shareArrow from "../../motion-assets/svgs/share.svg";
import {
  DefaultPostFooter,
  DefaultSharingIcons,
  DefaultLikeDuo,
  DefaultShareDuo,
  DefaultLikeBlock,
  DefaultPostLikes,
  DefaultLikeButton,
} from "../../styledComponents/styles";
import heartLiked from "../../motion-assets/svgs/heartLiked.svg";

function PostFooter({ post, amountOfLikes }) {
  const [postLike, setPostLike] = useState();
  const [amountLikes, setAmountLikes] = useState(amountOfLikes);

  const handleLikeButton = () => {
    LikePost();
    setPostLike(!postLike);
    setAmountLikes(amountLikes + 1);
    console.log("Liked!");
  };

  const LikePost = () => {
    let myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      `Bearer ${localStorage.getItem("accessToken")}`
    );

    myHeaders.append("Content-Type", "application/json");

    let requestOptions = {
      method: "POST",
      headers: myHeaders,

      redirect: "follow",
    };
    fetch(
      `https://motion.propulsion-home.ch/backend/api/social/posts/toggle-like/${post.id}`,

      requestOptions
    )
      .then(response => response.json())
      .then(result => {
        setPostLike(result.results);
        console.log(result);
      })
      .catch(error => console.log("error", error));
  };

  return (
    <>
      <DefaultPostFooter>
        <DefaultSharingIcons>
          <DefaultLikeDuo>
            <DefaultLikeButton onClick={handleLikeButton}>
              {postLike ? <img src={heartLiked} /> : <img src={heartLike} />}
            </DefaultLikeButton>
            {postLike ? <span>Liked</span> : <span>Like</span>}
          </DefaultLikeDuo>
          <DefaultShareDuo>
            <img src={shareArrow} />
            <span>Share</span>
          </DefaultShareDuo>
          <DefaultLikeBlock>
            <DefaultPostLikes> {`${amountLikes} Likes`}</DefaultPostLikes>
          </DefaultLikeBlock>
        </DefaultSharingIcons>
      </DefaultPostFooter>
    </>
  );
}

export default PostFooter;
