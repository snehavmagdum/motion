import styled from "styled-components";
import { colors, textsSizes, shadows } from "../../commonStyles/commonStyles";
import Button from "../Button";
import TagButton from "./TagButton";

const CardWrapper = styled.div`
  width: 100%;
  padding: 1.875rem 2.5rem;
  background: ${colors.mainWhite};
  box-shadow: ${shadows.profileCard};
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: 4px;
`;

const ProfileImageNotProvided = styled.div`
  border-radius: 50%;
  width: 95px;
  aspect-ratio: 1/1;
  margin-bottom: 1.2rem;
  background: ${colors.mainPurple};
`;
const UserName = styled.h3`
  font-size: ${textsSizes.h3.fontSize};
  font-weight: ${textsSizes.h3.fontWeight};
  margin-bottom: 0.5rem;
  white-space: nowrap;
`;
const CityAndCountry = styled.p`
  font-size: ${textsSizes.paragraph};
  margin-bottom: 2rem;
`;

const FlexWrapper = styled.div`
  display: flex;
  max-width: 100%;
  flex-wrap: wrap;
  margin-top: ${props => props.marginTop || null};
  margin-bottom: ${props => props.marginBottom || null};
  gap: ${props => props.gap || "1rem"};
  flex-direction: ${props => props.direction || null};
  padding: ${props => props.padding || null};
  justify-content: ${props => props.justify || null};
  align-items: ${props => props.align || null};
  border-top: ${props => props.borderStyle || null};
`;
const TextContent = styled.p`
  font-size: ${props => props.fontSize || textsSizes.paragraph};
  font-weight: ${props => props.fontWeight || null};
  text-align: ${props => props.textAlign || null};
`;

const thingsIlike = ["Cooking", "Travel", "Reading", "Swimming", "Running"];

const FriendInfoCard = ({ myData }) => {
  return (
    <CardWrapper>
      <ProfileImageNotProvided />
      <UserName>
        {myData?.first_name} {myData?.last_name}
      </UserName>
      <CityAndCountry>Leon, France</CityAndCountry>
      <FlexWrapper gap="0.5rem" marginBottom="2rem">
        <Button buttonLabel="FOLLOW" />
        <Button buttonLabel="ADD FRIEND" />
      </FlexWrapper>
      <TextContent textAlign="center">
        Lorem ipsum dolor sit amet, vim ut quas volumus probatus, has tantas
        laudem iracundia et, ad per utamur ceteros apeirian
      </TextContent>
      <FlexWrapper marginTop="2rem">
        {thingsIlike.map((item, idx) => (
          <TagButton key={idx}>{item}</TagButton>
        ))}
      </FlexWrapper>
    </CardWrapper>
  );
};

export default FriendInfoCard;
