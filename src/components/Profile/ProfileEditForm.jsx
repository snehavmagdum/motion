import { useState, useMemo } from "react";
import { axiosMotion } from "../../axios-instance/axios";
import styled from "styled-components";
import {
  colors,
  textsSizes,
  shadows,
  spacing,
  gradients,
} from "../../commonStyles/commonStyles";
import JaneSmithImage from "../../assets/images/jenneSmith-image.jpg";
import Button from "../Button";
import TagButton from "./TagButton";

const CardWrapper = styled.div`
  width: 100%;
  padding: 2.5rem 3.75rem;
  background: ${colors.mainWhite};
  box-shadow: ${shadows.profileCard};
  display: grid;
  grid-template-columns: 30% 1fr;
  border-radius: 4px;
`;
const ProfileImage = styled.img`
  border-radius: 50%;
  width: 95px;
  aspect-ratio: 1/1;
  margin-bottom: 1.2rem;
`;
const EmptyImage = styled.div`
  border-radius: 50%;
  width: 95px;
  aspect-ratio: 1/1;
  margin-bottom: 1.2rem;
  background: ${colors.mainPurple};
  display: flex;
  justify-content: center;
  align-items: center;
`;

const FlexWrapper = styled.div`
  display: flex;
  flex: 1;
  margin-top: ${props => props.marginTop || null};
  gap: ${props => props.gap || "1rem"};
  flex-direction: ${props => props.direction || null};
  padding: ${props => props.padding || null};
  justify-content: ${props => props.justify || null};
  align-items: ${props => props.align || null};
  border-top: ${props => props.borderStyle || null};
  grid-column-start: ${props => props.columnStart || null};
  grid-column-end: ${props => props.columnEnd || null};
  grid-row-start: ${props => props.rowStart || null};
  grid-row-end: ${props => props.rowEnd || null};
`;

const InputLable = styled.label`
  color: ${colors.black50};
`;
const InputField = styled.input`
  outline: none;
  border: none;
  border-bottom: solid 0.5px ${colors.fadedBlack};
  width: 100%;
  padding-bottom: 1rem;
  ::placeholder {
    color: ${colors.fadedBlack};
  }
`;
const TextContent = styled.p`
  font-size: ${props => props.fontSize || textsSizes.paragraph};
  font-weight: ${props => props.fontWeight || null};
  grid-column-start: 2;
  grid-column-end: 3;
  padding: ${spacing.profileInnerBoxesPaddings};
`;

const thingsIlike = ["Cooking", "Travel", "Reading", "Swimming", "Running"];

const ProfileEditForm = ({ editingMode, setEditingMode, userData }) => {
  const [firstName, setFirstName] = useState(userData?.first_name);
  const [lastName, setLastName] = useState(userData.last_name);
  const [username, setUserName] = useState(userData.username);
  const [location, setLocation] = useState(userData.location);
  const [email, setEmail] = useState(userData.email);
  const [phone, setPhone] = useState(userData.phone_number);
  const [about, setAbout] = useState(userData.about_me);
  const [password, setPassword] = useState();
  const [addTag, setAddTag] = useState();

  const handelSave = e => {
    e.preventDefault();
    axiosMotion.patch("/users/me/", {
      about_me: about,
      email: email,
      first_name: firstName,
      last_name: lastName,
      username: username,
      location: location,
      phone_number: phone,
      about_me: about,
    });
    setEditingMode(!editingMode);
  };

  return (
    <form>
      <CardWrapper>
        <FlexWrapper
          direction="column"
          padding={spacing.profileInnerBoxesPaddings}
          align="center"
        >
          {userData.avatar === null ? (
            <EmptyImage>
              <p
                style={{
                  fontSize: "1.5rem",
                  fontWeight: "800",
                  textTransform: "capitalize",
                  color: colors.mainWhite,
                }}
              >
                {firstName && firstName.slice(0, 1)}
              </p>
            </EmptyImage>
          ) : (
            <ProfileImage src={userData.avatar} />
          )}
          <Button buttonLabel="UPDATE IMAGE" />
        </FlexWrapper>
        <FlexWrapper
          direction="column"
          columnStart="1"
          columnEnd="2"
          rowStart="3"
          rowEnd="5"
          padding={`2.5rem ${spacing.profileInnerBoxesPaddings} 0`}
        >
          <Button buttonLabel="DELETE ACCOUNT" />
          <Button
            buttonLabel="SAVE"
            backgroundColor={gradients.mainButtonGradient}
            textColor={colors.mainWhite}
            onClickFunction={handelSave}
            type="submit"
          />
        </FlexWrapper>
        <FlexWrapper padding={spacing.profileInnerBoxesPaddings} gap="4rem">
          <FlexWrapper direction="column">
            <FlexWrapper direction="column" gap="1rem">
              <InputLable id="firstname">First Name</InputLable>
              <InputField
                name="firstname"
                placeholder="First name"
                type="text"
                onChange={e => setFirstName(e.target.value)}
                value={firstName}
              />
            </FlexWrapper>
            <FlexWrapper
              direction="column"
              gap="1rem"
              marginTop={spacing.formRowSpacing}
            >
              <InputLable id="email">Email</InputLable>
              <InputField
                name="email"
                placeholder="Email"
                type="email"
                onChange={e => setEmail(e.target.value)}
                value={email}
              />
            </FlexWrapper>
            <FlexWrapper
              direction="column"
              gap="1rem"
              marginTop={spacing.formRowSpacing}
            >
              <InputLable id="location">Location</InputLable>
              <InputField
                name="location"
                placeholder="Location"
                type="text"
                onChange={e => setLocation(e.target.value)}
                value={location}
              />
            </FlexWrapper>
            <FlexWrapper
              direction="column"
              gap="1rem"
              marginTop={spacing.formRowSpacing}
            >
              <InputLable id="about">About</InputLable>
              <InputField
                name="about"
                placeholder="About you"
                type="text"
                onChange={e => setAbout(e.target.value)}
                value={about}
              />
            </FlexWrapper>
          </FlexWrapper>
          <FlexWrapper direction="column">
            <FlexWrapper direction="column" gap="1rem">
              <InputLable id="lastname">Last Name</InputLable>
              <InputField
                name="lastname"
                placeholder="Last Name"
                type="text"
                onChange={e => setLastName(e.target.value)}
                value={lastName}
              />
            </FlexWrapper>
            <FlexWrapper
              direction="column"
              gap="1rem"
              marginTop={spacing.formRowSpacing}
            >
              <InputLable id="username">Username</InputLable>
              <InputField
                name="username"
                placeholder="Username"
                type="text"
                onChange={e => setUserName(e.target.value)}
                value={username}
              />
            </FlexWrapper>
            <FlexWrapper
              direction="column"
              gap="1rem"
              marginTop={spacing.formRowSpacing}
            >
              <InputLable id="phone">Phone</InputLable>
              <InputField
                name="phone"
                placeholder="+41"
                type="tel"
                onChange={e => setPhone(e.target.value)}
                value={phone}
              />
            </FlexWrapper>
            <FlexWrapper
              direction="column"
              gap="1rem"
              marginTop={spacing.formRowSpacing}
            >
              <InputLable id="password">Password</InputLable>
              <InputField
                name="password"
                type="password"
                onChange={e => setPassword(e.target.value)}
                value={password}
              />
            </FlexWrapper>
          </FlexWrapper>
        </FlexWrapper>
        <TextContent>Things I like</TextContent>
        <FlexWrapper
          padding={`0 ${spacing.profileInnerBoxesPaddings}`}
          columnStart="2"
          columnEnd="3"
        >
          {thingsIlike.map((item, idx) => (
            <TagButton key={idx}>{item}</TagButton>
          ))}
        </FlexWrapper>
        <FlexWrapper
          padding={spacing.profileInnerBoxesPaddings}
          columnStart="2"
          columnEnd="3"
        >
          <InputField
            name="tagText"
            placeholder="Type something..."
            type="text"
            onChange={() => {}}
            value={addTag}
          />
          <Button buttonLabel="ADD" />
        </FlexWrapper>
      </CardWrapper>
    </form>
  );
};

export default ProfileEditForm;
