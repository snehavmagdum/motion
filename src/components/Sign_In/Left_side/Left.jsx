import styled from "styled-components";
import background_image from "../../../../assets/images/background_image.png";
import back_img from "../../../../assets/images/logo_white.png";
import apple from "../../../../assets/svgs/apple.svg";
import google from "../../../../assets/svgs/google.svg";
import twitter from "../../../../assets/svgs/twitter_icon.svg";
import facebook from "../../../../assets/svgs/facebook_icon.svg";
import insta from "../../../../assets/svgs/instagram_icon.svg";

const LeftContainer = styled.div`
  padding: 40px;
  gap: 120px;
  width: 35%;
  height: 100%;
  background-image: url(${background_image}),
    linear-gradient(132.96deg, #c468ff 3.32%, #6e91f6 100%);
  background-blend-mode: multiply, normal;
  box-shadow: 0px 10px 30px rgba(0, 0, 0, 0.07);
  background-repeat: no-repeat;
  background-size: cover;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const LogoContainer = styled.div`
  /* background-color: white; */
  width: 260px;
  height: 270.35px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
`;

const Logo = styled.div`
  /* background-color: #d48484; */
  width: 207.36px;
  height: 125px;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 10px;
`;
const Text = styled.p`
  /* background-color: #d48484; */
  width: 207.36px;
  height: 35px;
  font-family: "Roboto";
  font-style: normal;
  font-weight: 500;
  font-size: 30px;
  line-height: 35px;
  text-align: center;
  letter-spacing: 0.75px;
  color: #ffffff;
`;

const Textp = styled(Text)`
  font-size: 10px;
  letter-spacing: 0px;
  font-weight: normal;
  line-height: normal;
`;

const LogoImage = styled.div`
  /* background: #ffffff; */
  box-shadow: 0px 10px 30px rgba(0, 0, 0, 0.07);
  width: 80px;
  height: 80px;
  fill: Solid #ffffff;
  border-radius: 30px;
`;

const StoreContainer = styled.div`
  /* background-color: #d48484; */
  width: 215.65px;
  height: 37.35px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 0px;
  gap: 12px;
  a {
    box-sizing: border-box;
    width: 98.29px;
    height: 37.35px;
    border: 1px solid rgba(255, 255, 255, 0.2);
    border-radius: 30px;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 10px;
    gap: 10px;
    flex-grow: 0;
  }
`;

const SocialLinkContainer = styled.div`
  gap: 40px;
  width: 250px;
  height: 94px;
  /* background-color: aqua; */
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: white;
`;

const SocialLinks = styled.div`
  /* background-color: #cfd8d8; */
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  opacity: 0.5;
  gap: 16px;
  width: 152px;
  height: 40px;
  a {
    img {
      width: 40px;
      height: 40px;
    }
  }
`;

const Left = () => {
  return (
    <LeftContainer>
      <LogoContainer>
        <Logo>
          <LogoImage>
            <img src={back_img} />
          </LogoImage>
          <Text>Motion</Text>
        </Logo>

        <Textp>
          Connect with friends and the world around you with Motion.
        </Textp>

        <StoreContainer>
          <a
            href="https://www.apple.com/de/store"
            rel="noreferrer"
            target="_blank"
          >
            <img src={apple} />
          </a>
          <a
            href="https://play.google.com/store/games"
            rel="noreferrer"
            target="_blank"
          >
            <img src={google} />
          </a>
        </StoreContainer>
      </LogoContainer>
      <SocialLinkContainer>
        <SocialLinks>
          <a href="https://twitter.com/" rel="noreferrer" target="_blank">
            <img src={twitter} />
          </a>
          <a
            href="https://de-de.facebook.com/"
            rel="noreferrer"
            target="_blank"
          >
            <img src={facebook} />
          </a>
          <a href="https://www.instagram.com/" rel="noreferrer" target="_blank">
            <img src={insta} />
          </a>
        </SocialLinks>
        <Textp>© Motion 2018. All rights reserved.</Textp>
      </SocialLinkContainer>
    </LeftContainer>
  );
};

export default Left;
