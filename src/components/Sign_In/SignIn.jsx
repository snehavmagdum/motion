import styled from "styled-components";
import Left from "./Left_side/Left";
import Right from "./Right_side/Right";
const MainContainer = styled.div`
  display: flex;
  width: 100vw;
  height: 100vh;
  /* background-color: #d5dc78; */
`;

const SignIn = () => {
  return (
    <MainContainer>
      <Left />
      <Right />
    </MainContainer>
  );
};

export default SignIn;
