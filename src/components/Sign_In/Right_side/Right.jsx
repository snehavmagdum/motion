import Login from "./Login";
import SignUp from "../../Sign_Up/SignUp";
import Congrats from "../../Sign_Up/Congrats";
import Verification from "../../Verification/Verifiation";

import { useState } from "react";
import Continue from "../../Verification/continue";

const Right = () => {
  const [render, setRender] = useState("login");

  if (render === "login") return <Login vari={render} func={setRender}></Login>;
  else if (render === "signup")
    return <SignUp vari={render} func={setRender}></SignUp>;
  else if (render === "congrats")
    return <Congrats vari={render} func={setRender}></Congrats>;
  else if (render === "verify")
    return <Verification vari={render} func={setRender}></Verification>;
  else if (render === "continue")
    return <Continue vari={render} func={setRender}></Continue>;
};

export default Right;
