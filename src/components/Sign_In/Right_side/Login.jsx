import styled from "styled-components";
import avatar from "../../../../assets/svgs/avatar.svg";
import password from "../../../../assets/svgs/password.svg";

import { useState } from "react";
import { AxiosMotion } from "../../../axios";
import { useDispatch } from "react-redux";
import { loginUser } from "../../../store/Slices/UserSlice";
import { updateAxiosConfig } from "../../../axios";

export const RightContainer = styled.div`
  width: 65%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 20px;
  /* background-color: #78a7dd; */
`;
export const SignUpButton = styled.div`
  /* background-color: #dbdee7; */
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  padding: 0px;
  gap: 18px;

  width: 100%;
  height: 40px;
`;
export const StyledButton = styled.div`
  display: flex;
  align-items: center;
  padding: 14px 25px;
  gap: 10px;
  border-radius: 100px;
  border: 1px solid rgba(0, 0, 0, 0.2);
  width: 120px;
  height: 40px;
`;

export const MainContainerForm = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 340px;
  height: 80%;
`;

export const FormContainer = styled.form`
  /* background-color: #f0c2c2; */
  width: 300px;
  height: 350px;
  display: flex;
  flex-direction: column;
  align-items: center;
  border: none;
  gap: 40px;
`;
export const Text = styled.p`
  width: fit-content;
  height: 47px;
  align-self: center;
  font-family: "Roboto";
  font-style: normal;
  font-weight: 500;
  font-size: 40px;
  line-height: 47px;
  gap: 40px;
`;

export const InputEmail = styled.input`
  background-image: url(${avatar});
  background-repeat: no-repeat;
  background-size: 30px;

  display: flex;
  flex-direction: row;
  align-items: flex-start;
  padding-left: 45px;
  gap: 22px;
  border: none;
  outline: none;
  width: 100%;
  height: 42px;
  :focus {
    border-bottom: 1px solid rgb(211, 209, 209);
  }
`;
const InputPass = styled.input`
  background-image: url(${password});
  background-repeat: no-repeat;
  background-size: 25px;
  border: none;
  outline: none;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  padding-left: 45px;
  gap: 22px;

  width: 100%;
  height: 42px;
  :focus {
    border-bottom: 1px solid rgb(211, 209, 209);
  }
`;

export const StyledButtonSignIn = styled.button`
  width: 288px;
  height: 60px;
  border: none;

  background: linear-gradient(132.96deg, #c468ff 3.32%, #6e91f6 100%);
  border-radius: 30px;
`;
const Msg = styled.p`
  color: red;
`;

const Login = ({ vari, func }) => {
  // console.log(vari, func);
  const [email, setEmail] = useState("dhk65452@zslsz.com");
  const [password, setPassword] = useState("asdfg");
  const [msg, setMsg] = useState("");

  // const token = useSelector((store) => store.user.accessToken);
  // console.log("token", token);
  const dispatch = useDispatch();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      // getting taken using user credentials
      const res = await AxiosMotion.post("/auth/token/", { email, password });
      console.log("Response getting access token :", res.data.access);
      // storing token in store
      dispatch(loginUser(res.data.access));

      // storing token in localstorage
      localStorage.setItem("accessToken", res.data.access);
      const accessToken = localStorage.getItem("accessToken");
      updateAxiosConfig(accessToken);
    } catch (e) {
      setMsg("Invalid User Name or Password");
      console.log(e);
    }
  };

  // const handleFetching = async (e) => {
  //   e.preventDefault();
  //   try {
  //     // getting taken using user credentials
  //     const res = await AxiosMotion.get("/users/me/", { email, password });
  //     console.log("Response getting access token :", res);
  //     // storing token in store
  //     // dispatch(loginUser(res.data.access));

  //     // storing token in localstorage
  //     // localStorage.setItem("accessToken", res.data.access);
  //     // const accessToken = localStorage.getItem("accessToken");
  //     // updateAxiosConfig(accessToken);
  //   } catch (e) {
  //     console.log(e);
  //   }
  // };

  const handleRender = async () => {
    vari = "signup";
    func(vari);
    //---------------------------------------------
    // e.preventDefault();
    // try {
    //   // getting taken using user credentials
    //   const res = await AxiosMotion.get("/users/me", { email, password });
    //   console.log("Response getting access token :", res);
    // } catch (e) {
    //   console.log(e);
    // }
  };

  return (
    <RightContainer>
      <SignUpButton>
        <p>Dont have an account?</p>
        <StyledButton onClick={handleRender}>SignUp</StyledButton>
      </SignUpButton>
      <MainContainerForm>
        <FormContainer onSubmit={handleSubmit}>
          <Text>Sign In</Text>

          <InputEmail
            type="email"
            placeholder="E-mail"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          ></InputEmail>
          <InputPass
            type="password"
            value={password}
            placeholder="Password"
            onChange={(e) => setPassword(e.target.value)}
          ></InputPass>
          {msg ? <Msg>{msg}</Msg> : null}
        </FormContainer>
        <StyledButtonSignIn type="submit" onClick={handleSubmit}>
          Sign In
        </StyledButtonSignIn>
      </MainContainerForm>
    </RightContainer>
  );
};

export default Login;
