import styled from "styled-components";
import { colors } from "../commonStyles/commonStyles";

const ButtonStyle = styled.button`
  padding: 1rem 2rem;
  font-size: 0.7rem;
  border-radius: 30px;
  border: solid 0.5px ${colors.fadedBlack};
  background: ${props => props.backgroundColor || "transparent"};
  color: ${props => props.textColor || colors.mainBlack};
  margin-top: ${props => props.marginTop || null};
  margin-bottom: ${props => props.marginBottom || null};
  white-space: nowrap;
`;

const Button = ({
  buttonLabel,
  backgroundColor,
  textColor,
  marginTop,
  marginBottom,
  onClickFunction,
  type,
}) => {
  return (
    <ButtonStyle
      backgroundColor={backgroundColor}
      textColor={textColor}
      marginTop={marginTop}
      marginBottom={marginBottom}
      onClick={onClickFunction}
      type={type}
    >
      {buttonLabel}
    </ButtonStyle>
  );
};

export default Button;
