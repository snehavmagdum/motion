import styled from "styled-components";

import { Text } from "../Sign_In/Right_side/Login";
import { StyledButtonSignIn } from "../Sign_In/Right_side/Login";
import { RightContainer } from "../Sign_In/Right_side/Login";
import { AxiosInstance } from "../../axios";
import { useState } from "react";

// import { useState } from "react";

const RightContainerVerify = styled(RightContainer)`
  justify-content: center;
`;

const MainContainerVerify = styled.div`
  align-self: center;
  width: 70%;
  height: 60%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 40px;
  /* background-color: aquamarine; */
`;

const FormContainerVerify = styled.form`
  width: 500px;
  height: 250px;
  /* background-color: #c68f8f; */
  display: flex;
  margin-left: 40px;
  flex-wrap: wrap;
  gap: 20px;
  padding-top: 10px;
`;
const UserName = styled.div`
  width: 500px;
  height: fit-content;
  /* background-color: #78db80; */
  display: flex;
  justify-content: space-between;
  /* flex-direction: column; */
  flex-wrap: wrap;

  /* padding-top: 10px; */
`;

const InputValidate = styled.input`
  width: 450px;
  height: 25px;
  border: none;
  outline: none;
  ::placeholder {
    color: black;
  }
  :focus {
    border-bottom: 1px solid rgb(211, 209, 209);
  }
`;
const InputField = styled.input`
  width: 240px;
  height: 25px;
  border: none;
  outline: none;
  ::placeholder {
    color: black;
  }

  :focus {
    border-bottom: 1px solid rgb(211, 209, 209);
  }
`;

const Label = styled.label`
  width: 240px;
  height: 25px;
  color: #949090;
  font-size: small;
  display: flex;
  align-items: flex-end;
  :focus {
    border-bottom: 1px solid rgb(211, 209, 209);
  }
`;
const Msg = styled.p`
  color: red;
`;

const Verification = ({ vari, func }) => {
  const [msg, setMsg] = useState("");
  const [validationCode, setValidationCode] = useState("");
  const [email, setEmail] = useState("");
  const [userName, setUserName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [password, setPassword] = useState("");
  const [repeatPassword, setRepeatPassword] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      // getting taken using user credentials
      const res = await AxiosInstance.patch("/auth/registration/validation/", {
        email: email,
        username: userName,
        code: validationCode,
        password: password,
        password_repeat: repeatPassword,
        first_name: firstName,
        last_name: lastName,
      });
      console.log("Registration :", res);
      console.log(
        validationCode,
        email,
        userName,
        firstName,
        lastName,
        password,
        repeatPassword
      );
      vari = "continue";
      func(vari);
    } catch (e) {
      setMsg("Verifivation failed please enter valid data");
      console.log(e);
    }
  };

  return (
    <RightContainerVerify>
      <MainContainerVerify>
        <Text>Verification</Text>
        <FormContainerVerify onSubmit={handleSubmit}>
          <InputValidate
            placeholder="Validation code"
            value={validationCode}
            onChange={(e) => setValidationCode(e.target.value)}
          ></InputValidate>
          <UserName>
            <Label>E-mail</Label>
            <Label>User name</Label>
            <InputField
              type="email"
              placeholder="mark@gmail.com"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            ></InputField>
            <InputField
              type="text"
              placeholder="User name"
              value={userName}
              onChange={(e) => setUserName(e.target.value)}
            ></InputField>
          </UserName>
          <InputField
            type="text"
            placeholder="First name"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
          ></InputField>
          <InputField
            type="text"
            placeholder="Last name"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
          ></InputField>
          <InputField
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          ></InputField>
          <InputField
            type="password"
            placeholder="Repeat Password"
            value={repeatPassword}
            onChange={(e) => setRepeatPassword(e.target.value)}
          ></InputField>
          {msg ? <Msg>{msg}</Msg> : null}
        </FormContainerVerify>
        <StyledButtonSignIn type="submit" onClick={handleSubmit}>
          Continue
        </StyledButtonSignIn>
      </MainContainerVerify>
    </RightContainerVerify>
  );
};

export default Verification;
