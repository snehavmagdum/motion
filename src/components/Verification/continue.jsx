import styled from "styled-components";

import { MainContainer } from "../Sign_Up/SignUp";
import { FormContainer } from "../Sign_In/Right_side/Login";
import { Text } from "../Sign_In/Right_side/Login";

import { RightContainer } from "../Sign_In/Right_side/Login";

// import { useState } from "react";

export const FormContainerCongrats = styled(FormContainer)`
  height: max-content;
  justify-content: space-between;
`;

const Continue = () => {
  return (
    <RightContainer>
      <MainContainer>
        <FormContainerCongrats>
          <Text>Congratulations Verification is successful</Text>
        </FormContainerCongrats>
      </MainContainer>
    </RightContainer>
  );
};

export default Continue;
