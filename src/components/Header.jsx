import styled from "styled-components";
import { colors, shadows, textsSizes } from "../commonStyles/commonStyles";
import motion_logo from "../../assets/images/logo.png";

const HeaderWrapper = styled.div`
  width: 100%;

  padding: 1rem 1.75rem;
  background: ${colors.mainWhite};

  border: 1px solid black;
  display: flex;
  justify-content: space-between;
  border-radius: 4px;
  gap: 40px;
`;
const LeftComponents = styled.div`
  width: 35%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  gap: 100px;
`;
const Logo = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
  width: 40%;
  height: 36px;
`;

const MotionLogo = styled.div`
  width: 26px;
  height: 26px;
  background-image: url(${motion_logo});
`;
const Text = styled.div`
  width: 69px;
  height: 26px;
  text-align: center;
`;

const Head = styled.h3`
  font-weight: ${textsSizes.h3.fontWeight};
  font-size: 22px;
`;

const PostsFriends = styled.div`
  width: 50%;
  height: 24px;
  display: flex;
  align-items: center;
  justify-content: space-around;
`;

const LogoPosts = styled.div`
  width: 81px;
  height: 24px;
  display: flex;
  justify-content: space-around;
`;

const Plogo = styled.div`
  opacity: 30%;

  width: 24px;
  height: 24px;
`;
const Ptext = styled.p``;

const Flogo = styled.div`
  background-size: cover;
  width: 24px;
  height: 24px;
`;

const RightComponents = styled.div`
  width: 10%;
  display: flex;
  justify-content: space-around;
  align-items: center;
  gap: 10px;
`;

const Notification = styled.div`
  width: 42px;
  height: 33px;
  padding-top: 5px;
`;
const ProfilePic = styled.div`
  width: 40px;
  height: 40px;
`;
const Menu = styled.div`
  width: 24px;
  height: 24px;
  padding-top: 5px;
`;

const Header = () => {
  return (
    <HeaderWrapper>
      <LeftComponents>
        <Logo>
          <MotionLogo></MotionLogo>
          <Text>
            <Head>Motion</Head>
          </Text>
        </Logo>
        <PostsFriends>
          <LogoPosts>
            <Plogo>
              <img src="../../assets/svgs/posts_logo.svg" />
            </Plogo>
            <Ptext>Posts</Ptext>
          </LogoPosts>
          <LogoPosts>
            <Flogo>
              <img src="../../assets/svgs/icon-friends.svg" />
            </Flogo>
            <Ptext>Friends</Ptext>
          </LogoPosts>
        </PostsFriends>
      </LeftComponents>
      <RightComponents>
        <Notification>
          <img src="../../assets/svgs/notification_bell.svg" />
        </Notification>
        <ProfilePic>
          <img src="../../assets/images/users/jennifer.png" />
        </ProfilePic>
        <Menu>
          <img src="../../assets/svgs/menu.svg" />
        </Menu>
      </RightComponents>
    </HeaderWrapper>
  );
};

export default Header;
