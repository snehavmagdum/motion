import React from "react";
import "./styles.css";
import search_icon from "../../motion-assets/svgs/search_icon.svg";

function SearchBar() {
  return (
    <>
      <div className="searchBlock">
        <div className="searchSpan">
          <img className="searchIcon" src={search_icon} alt="search icon" />
          <span className="searchSpan">Search post...</span>
        </div>
        <ul className="searchBarLikedFriendsFollow">
          <li className="singleLi">Liked</li>
          <li>Friends</li>
          <li>Follow</li>
        </ul>
      </div>
    </>
  );
}

export default SearchBar;
