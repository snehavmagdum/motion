import styled from "styled-components";

import { MainContainer } from "./SignUp";
import { FormContainer } from "../Sign_In/Right_side/Login";
import { Text } from "../Sign_In/Right_side/Login";
import { StyledButtonSignIn } from "../Sign_In/Right_side/Login";
import { RightContainer } from "../Sign_In/Right_side/Login";
import checkmark from "../../../assets/svgs/checkmark.svg";
import { useSelector } from "react-redux";

// import { useState } from "react";

export const FormContainerCongrats = styled(FormContainer)`
  height: max-content;
  justify-content: space-between;
`;

const Icon = styled.div`
  background-image: url(${checkmark});
  width: 81px;
  height: 81px;
`;

const Label = styled.label`
  color: #949090;
  font-size: small;
  text-align: center;
`;

const Congrats = ({ vari, func }) => {
  const email = useSelector((store) => store.user.email);

  const handleSubmit = async (e) => {
    e.preventDefault();
    vari = "verify";
    func(vari);
  };

  return (
    <RightContainer>
      <MainContainer>
        <FormContainerCongrats onSubmit={handleSubmit}>
          <Text>Congratulations</Text>
          <Icon></Icon>
          <Label>We’ve sent a confirmation code to your email {email}</Label>
        </FormContainerCongrats>
        <StyledButtonSignIn type="submit" onClick={handleSubmit}>
          Continue
        </StyledButtonSignIn>
      </MainContainer>
    </RightContainer>
  );
};

export default Congrats;
