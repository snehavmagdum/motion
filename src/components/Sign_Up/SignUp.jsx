import styled from "styled-components";
import { FormContainer, MainContainerForm } from "../Sign_In/Right_side/Login";
import { InputEmail } from "../Sign_In/Right_side/Login";
import { Text } from "../Sign_In/Right_side/Login";
import { StyledButtonSignIn } from "../Sign_In/Right_side/Login";
import letter from "../../../assets/svgs/letter.svg";
import { RightContainer } from "../Sign_In/Right_side/Login";
import { SignUpButton } from "../Sign_In/Right_side/Login";
import { StyledButton } from "../Sign_In/Right_side/Login";

import { useState } from "react";
import { AxiosInstance, AxiosMotion } from "../../axios";
import { useDispatch } from "react-redux";
import { setemail } from "../../store/Slices/UserSlice";

const TopDiv = styled(SignUpButton)``;

export const MainContainer = styled(MainContainerForm)`
  /* background-color: #c8aaaa; */
  gap: 50px;
`;

export const InputEmailSignUp = styled(InputEmail)`
  height: 30px;
  /* padding-left: 30px; */
  /* background-color: #c6d698; */
  background-size: 30px;

  background-image: url(${letter});
  display: flex;
  flex-direction: column;
`;

export const FormContainerSignUp = styled(FormContainer)`
  /* background-color: gray; */
  height: 250px;
  gap: 4px;
`;
export const Label = styled.label`
  color: #a29e9e;

  align-self: flex-start;
  margin-top: 50px;
  margin-left: 35px;
`;
const Msg = styled.p`
  color: red;
`;

const SignUp = ({ vari, func }) => {
  const [msg, setMsg] = useState("");
  const [email, setEmail] = useState("");

  const dispatch = useDispatch();
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      // getting taken using user credentials
      const res = await AxiosInstance.post("/auth/registration/", { email });
      console.log("Registration :", res);
      dispatch(setemail(email));

      console.log("Registration Done");
      vari = "congrats";
      func(vari);
    } catch (e) {
      setMsg("Please enter valid E-mail address");
      console.log(e);
    }
  };
  const handleRender = () => {
    vari = "login";
    func(vari);
  };

  return (
    <RightContainer>
      <TopDiv>
        <p>Already have an account?</p>
        <StyledButton onClick={handleRender}>SignIn</StyledButton>
      </TopDiv>
      <MainContainer>
        <FormContainerSignUp onSubmit={handleSubmit}>
          <Text>Sign up</Text>
          <Label>E-mail</Label>
          <InputEmailSignUp
            type="email"
            // placeholder="E-mail"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          ></InputEmailSignUp>
          {msg ? <Msg>{msg}</Msg> : null}
        </FormContainerSignUp>
        <StyledButtonSignIn type="submit" onClick={handleSubmit}>
          Sign up
        </StyledButtonSignIn>
      </MainContainer>
    </RightContainer>
  );
};

export default SignUp;
