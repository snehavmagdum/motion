import React from "react";
import { useState } from "react";
import "./styles.css";
import { RiCloseLine } from "react-icons/ri";
import jennifer from "../../motion-assets/images/users/jennifer.png";
import sendButton from "../../motion-assets/svgs/send_button.svg";
import postShape from "../../motion-assets/svgs/postShape.svg";

import {
  DefaultDarkBackground,
  DefaultCenteredModal,
  DefaultModalThumbWhats,
  DefaultbuttonModalArrowBox,
  DefaultModalFooter,
} from "../../styledComponents/styles";

function ModalPost({ onClickFuntion, isOpen }) {
  const [change, setChange] = useState("");
  const [inputImage, setInputImage] = useState([]);
  const [inputVisible, setInputVisible] = useState(false);

  console.log(inputImage);

  const PostPost = () => {
    let myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      `Bearer ${localStorage.getItem("accessToken")}`
    );

    myHeaders.append("Content-Type", "application/json");
    let raw = JSON.stringify({
      content: change,
    });
    let requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch(
      "https://motion.propulsion-home.ch/backend/api/social/posts/",

      requestOptions
    )
      .then(response => response.json())
      .then(result => {
        console.log(result);
      })
      .catch(error => console.log("error", error));
  };

  const handleImageClick = () => {
    setInputVisible(true);
  };

  const handleModalClosure = () => {
    setIsOpen(!isOpen);
    console.log("click");
  };

  return (
    <DefaultDarkBackground style={{ display: !isOpen && "none" }}>
      <DefaultCenteredModal>
        <DefaultModalThumbWhats>
          <img src={jennifer} alt="post-feed" />
          <form>
            <input
              onChange={e => {
                setChange(e.target.value);
              }}
              type="text"
              className="postPrompt"
              placeholder="What's on your mind, Jennifer?"
            ></input>
          </form>
          <button className="closeModalIcon" onClick={onClickFuntion}>
            <RiCloseLine />
          </button>
        </DefaultModalThumbWhats>
        <DefaultModalFooter>
          <div className="modalAddPicture">
            <img src={postShape} onClick={handleImageClick} />

            {inputVisible && (
              <div>
                <input
                  type="file"
                  onChange={e => {
                    setInputImage(e.target.value);
                  }}
                />
              </div>
            )}
          </div>
          <DefaultbuttonModalArrowBox>
            <button onClick={PostPost} className="sendPostButton">
              <img className="postArrow" src={sendButton} />
            </button>
          </DefaultbuttonModalArrowBox>
        </DefaultModalFooter>
      </DefaultCenteredModal>
    </DefaultDarkBackground>
  );
}

export default ModalPost;
