import { useState, useEffect } from "react";
import "./styles.css";
import albert from "../../motion-assets/images/users/alber.png";
import large_image from "../../motion-assets/images/feedPics/large_image.png";
import leticia from "../../motion-assets/images/users/leticia.png";
import PostFooter from "../postFooter/PostFooter";
import FriendUserPost from "./FriendUsersPosts";
import MySinglePost from "./MySinglePost";
import {
  DefaultPostColumn,
  DefaultPostMainContainer,
  DefaultWritePost,
  DefaultUserPicturePost,
  DefaultPicNameNow,
  DefaultUserNameTiming,
  DefaultUserName,
  DefaultPostTiming,
  DefaultPostText,
} from "../../styledComponents/styles";
import { useFetch } from "../../Hooks/useFetch";

import WritePost from "./WritePost";

function PostContainer() {
  const [postList, setPostList] = useState([]);
  useEffect(() => {
    GetOldPosts();
  }, []);

  const GetOldPosts = () => {
    let myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      `Bearer ${localStorage.getItem("accessToken")}`
    );

    let requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };
    fetch(
      "https://motion.propulsion-home.ch/backend/api/social/posts/",
      requestOptions
    )
      .then(response => response.json())
      .then(result => {
        setPostList(result.results);
      })
      .catch(error => console.log("error", error));
  };

  const userInfo = useFetch("/users/me/");

  return (
    <>
      {/* right column content starts */}
      <DefaultPostMainContainer>
        <DefaultPostColumn>
          <WritePost data={userInfo} />
          <MySinglePost />
          {/* right column content ends */}
        </DefaultPostColumn>

        {/* left column content starts */}
        <div className="postColumn">
          {postList?.map(post => {
            return <FriendUserPost key={post.id} post={post} />;
          })}

          <DefaultUserPicturePost>
            <DefaultPicNameNow>
              <div className="userPic">
                <img src={albert} className="userThumb" alt="Jennifer-feed" />
              </div>

              <DefaultUserNameTiming>
                <DefaultUserName>Albert Lawrence</DefaultUserName>
                <DefaultPostTiming>June 20</DefaultPostTiming>
              </DefaultUserNameTiming>
              <div className="userSharedAPostDiv">
                <span className="userSharedAPost">shared a post</span>
              </div>
            </DefaultPicNameNow>
            <div className="userStatus">
              <DefaultPostText>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris
                odio eros, ornare et sodales rutrum...
              </DefaultPostText>
            </div>
            <div className="sharedPost">
              <DefaultPicNameNow>
                <div className="userPic">
                  <img
                    src={leticia}
                    className="userThumb"
                    alt="Jennifer-feed"
                  />
                </div>
                <DefaultUserNameTiming>
                  <DefaultUserName>Leticia Suárez</DefaultUserName>
                  <DefaultPostTiming>June 19</DefaultPostTiming>
                </DefaultUserNameTiming>
              </DefaultPicNameNow>
              <div className="userStatus">
                <DefaultPostText>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Mauris odio eros, ornare et sodales rutrum...
                </DefaultPostText>
              </div>

              <div className="singlepostGrid">
                <img src={large_image} />
              </div>
            </div>
            <PostFooter post={postList} />
          </DefaultUserPicturePost>
          {/* copy albert pasted ends starts */}
        </div>
        {/* left column content ends */}
      </DefaultPostMainContainer>
    </>
  );
}

export default PostContainer;
