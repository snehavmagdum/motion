import "./styles.css";
import {
  DefaultUserPicturePost,
  DefaultPicNameNow,
  DefaultUserNameTiming,
  DefaultUserName,
  DefaultPostTiming,
  DefaultPostText,
  DefaultUserStatus,
  DefaultPictureGrid,
  DefaultSinglePictureStyle,
} from "../../styledComponents/styles";
import PostFooter from "../postFooter/PostFooter";
import { colors } from "../../commonStyles/commonStyles";

function FriendUserPost({ post }) {
  const isoStr = post.created;
  const date = new Date(isoStr);
  const dateString = date.toString().slice(0, 24);
  console.log(isoStr);
  console.log(typeof dateString, "converted date");

  return (
    <>
      <DefaultUserPicturePost>
        <DefaultPicNameNow>
          <div
            className="userPic"
            style={{
              display: "flex",
              justifyContent: "Center",
              alignItems: "center",
            }}
          >
            {post?.user.avatar === null ? (
              <p
                style={{
                  fontSize: "1rem",
                  fontWeight: "800",
                  textTransform: "capitalize",
                  color: colors.mainWhite,
                  background: colors.mainPurple,
                  width: "2rem",
                  borderRadius: "50%",
                  textAlign: "center",
                  lineHeight: "2rem",
                }}
                className="userThumb"
              >
                {post?.user.first_name && post.user.first_name.slice(0, 1)}
              </p>
            ) : (
              <img src={post?.user.avatar} className="userThumb" />
            )}
          </div>
          <DefaultUserNameTiming>
            <DefaultUserName>
              {post.user?.first_name} {post.user?.last_name}
            </DefaultUserName>
            <DefaultPostTiming>{dateString}</DefaultPostTiming>
          </DefaultUserNameTiming>
        </DefaultPicNameNow>
        <DefaultUserStatus>
          <DefaultPostText>{post?.content}</DefaultPostText>
        </DefaultUserStatus>
        {post?.images ? (
          <p>
            {post.images.map((image, index) => {
              return (
                <DefaultSinglePictureStyle src={image.image} key={index} />
              );
            })}
          </p>
        ) : (
          "<p>user has no image</p>"
        )}
        <PostFooter post={post} amountOfLikes={post.amount_of_likes} />
      </DefaultUserPicturePost>
    </>
  );
}

export default FriendUserPost;
