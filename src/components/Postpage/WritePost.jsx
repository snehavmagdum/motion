import React from "react";
import { useState } from "react";
import "./styles.css";
import jennifer from "../../motion-assets/images/users/jennifer.png";
import sendButton from "../../motion-assets/svgs/send_button.svg";
import ModalPost from "./ModalPost";
import { DefaultWritePost } from "../../styledComponents/styles";
import { colors } from "../../commonStyles/commonStyles";

function WritePost({ data }) {
  const [change, setChange] = useState("");
  const [isOpen, setIsOpen] = useState(false);

  const PostPost = () => {
    let myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      `Bearer ${localStorage.getItem("accessToken")}`
    );

    myHeaders.append("Content-Type", "application/json");

    let raw = JSON.stringify({
      content: change,
    });

    let requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    fetch(
      "https://motion.propulsion-home.ch/backend/api/social/posts/",

      requestOptions
    )
      .then(response => response.json())
      .then(result => {
        setChange(result.results);
        console.log(result);
      })
      .catch(error => console.log("error", error));
  };

  console.log(isOpen);

  return (
    <DefaultWritePost onClick={() => setIsOpen(true)}>
      <div className="thumbWhats">
        <div>
          {data?.avatar === null ? (
            <p
              style={{
                fontSize: "1rem",
                fontWeight: "800",
                textTransform: "capitalize",
                color: colors.mainWhite,
                background: colors.mainPurple,
                width: "2rem",
                borderRadius: "50%",
                textAlign: "center",
                lineHeight: "2rem",
              }}
            >
              {data?.first_name && data?.first_name.slice(0, 1)}
            </p>
          ) : (
            <img src={data?.avatar} />
          )}
        </div>
        <form>
          <input
            onChange={e => {
              setChange(e.target.value);
            }}
            type="text"
            className="postPrompt"
            placeholder="What's on your mind, Jennifer?"
          ></input>
        </form>
      </div>
      <div className="buttonArrowBox">
        {/* <button onClick={PostPost} className="sendPostButton">
              <img className="postArrow" src={sendButton} />
            </button> */}
      </div>
      {isOpen && (
        <ModalPost onClickFuntion={() => setIsOpen(!isOpen)} isOpen={isOpen} />
      )}
    </DefaultWritePost>
  );
}

export default WritePost;
