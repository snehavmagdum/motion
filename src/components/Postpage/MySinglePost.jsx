import { useState, useEffect } from "react";
import { colors, gradients, shadows } from "../../commonStyles/commonStyles";
import "./styles.css";
import {
  DefaultUserPicturePost,
  DefaultPicNameNow,
  DefaultUserNameTiming,
  DefaultUserName,
  DefaultPostTiming,
  DefaultPostText,
  DefaultUserStatus,
  DefaultPictureGrid,
} from "../../styledComponents/styles";
import PostFooter from "../postFooter/PostFooter";
import jennifer from "../../motion-assets/images/users/jennifer.png";

function MySinglePost() {
  const [showMypost, setShowMyPost] = useState([]);

  useEffect(() => {
    ShowMyPost();
  }, []);

  const ShowMyPost = () => {
    let myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      `Bearer ${localStorage.getItem("accessToken")}`
    );

    myHeaders.append("Content-Type", "application/json");

    let raw = JSON.stringify({
      content: showMypost,
    });

    let requestOptions = {
      method: "GET",
      headers: myHeaders,

      redirect: "follow",
    };
    fetch(
      "https://motion.propulsion-home.ch/backend/api/social/posts/me",

      requestOptions
    )
      .then(response => response.json())
      .then(result => {
        setShowMyPost(result.results);
        console.log(result);
      })
      .catch(error => console.log("error", error));
  };

  return (
    <>
      {showMypost?.map((post, index) => (
        <DefaultUserPicturePost key={index}>
          <DefaultPicNameNow>
            <div>
              {post?.user.avatar === null ? (
                <p
                  style={{
                    fontSize: "1rem",
                    fontWeight: "800",
                    textTransform: "capitalize",
                    color: colors.mainWhite,
                    background: colors.mainPurple,
                    width: "2rem",
                    borderRadius: "50%",
                    textAlign: "center",
                    lineHeight: "2rem",
                  }}
                >
                  {post?.user.first_name && post?.user.first_name.slice(0, 1)}
                </p>
              ) : (
                <img src={post?.user.avatar} />
              )}
            </div>

            <DefaultUserNameTiming>
              <DefaultUserName>
                {post.user.first_name} &nbsp;
                {post.user.last_name}
              </DefaultUserName>
              <DefaultPostTiming>{post.created}</DefaultPostTiming>
            </DefaultUserNameTiming>
          </DefaultPicNameNow>
          <DefaultUserStatus>
            <DefaultPostText>{post.content}</DefaultPostText>
          </DefaultUserStatus>
          <DefaultPictureGrid>
            {/* <div className="cell" id="1">
              <img src={image1} />
            </div>
            <div className="cell" id="2">
              <img src={image2} />
            </div>
            <div className="cell" id="3">
              <img src={image3} />
            </div>
            <div className="cell" id="4">
              <img src={image4} />
            </div> */}
          </DefaultPictureGrid>

          <PostFooter post={post} amountOfLikes={post.amount_of_likes} />
        </DefaultUserPicturePost>
      ))}
    </>
  );
}

export default MySinglePost;
